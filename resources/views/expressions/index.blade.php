@extends('layouts.app')

@section('content')

    <h2>@lang('All expressions'):</h2>

        @foreach($expressions as $expression)
            <p><a href="{{route('expressions.show', ['expression' => $expression])}}">{{$expression->content}}</a></p>
        @endforeach

    @auth()
    <hr>
    <h4 class="mt-5">@lang('Translation order'):</h4>
    <form method="post" action="{{route('expressions.store')}}" class="w-25 mt-3 mb-5">
        @csrf
        <div class="form-group">
            <label for="content">@lang('Write an expression'):</label>
            <textarea class="form-control @error('content') is-invalid @enderror" id="content" name="content" rows="3"></textarea>
            @error('content')
            <p class="text-danger">{{ $message }}</p>
            @enderror
        </div>
        <button type="submit" class="btn btn-success">@lang('Order translation')</button>
    </form>
    @endauth

@endsection
