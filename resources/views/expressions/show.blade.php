@extends('layouts.app')

@section('content')

    @can('viewIfNotTranslation', $expression)
        <p class="text-center h4">@lang('No translation')</p>
    @endcan

    @can('view', $expression)
        <h4 class="mb-3">@lang('Translation of the expression') "{{$expression->content}}":</h4>
        <div>
            @if($locale == "en")
                <p>@lang('Russian'): <span class="font-weight-bold">{{$expression_ru}}</span></p>
            @elseif($locale == "ru")
                <p>@lang('English'): <span class="font-weight-bold">{{$expression_en}}</span></p>
            @endif
            <p>@lang('German'): <span class="font-weight-bold">{{$expression_de}}</span></p>
            <p>@lang('French'): <span class="font-weight-bold">{{$expression_fr}}</span></p>
            <p>@lang('Japanese'): <span class="font-weight-bold">{{$expression_ja}}</span></p>
            <p>@lang('Spanish'): <span class="font-weight-bold">{{$expression_sp}}</span></p>
        </div>

        @if(Auth::check())
            <hr>
            <h4 class="mt-5">@lang('Translate'):</h4>
            <form method="post" action="{{route('expressions.update', ['expression' => $expression])}}" class="w-50 mt-3 mb-5">
                @csrf
                @method('put')

                @if($locale == "en")
                    <div class="form-group">
                        <label for="content">@lang('Russian'):</label>
                        <textarea class="form-control @error('content') is-invalid @enderror" id="content" name="content:ru" rows="1">{{$expression_ru}}</textarea>
                        @error('content')
                        <p class="text-danger">{{ $message }}</p>
                        @enderror
                    </div>

                @elseif($locale == "ru")
                    <div class="form-group">
                        <label for="content">@lang('English'):</label>
                        <textarea class="form-control @error('content') is-invalid @enderror" id="content" name="content:en" rows="1">{{$expression_en}}</textarea>
                        @error('content')
                        <p class="text-danger">{{ $message }}</p>
                        @enderror
                    </div>
                @endif

                <div class="form-group">
                    <label for="content">@lang('German'):</label>
                    <textarea class="form-control @error('content') is-invalid @enderror" id="content" name="content:de" rows="1">{{$expression_de}}</textarea>
                    @error('content')
                    <p class="text-danger">{{ $message }}</p>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="content">@lang('French'):</label>
                    <textarea class="form-control @error('content') is-invalid @enderror" id="content" name="content:fr" rows="1">{{$expression_fr}}</textarea>
                    @error('content')
                    <p class="text-danger">{{ $message }}</p>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="content">@lang('Japanese'):</label>
                    <textarea class="form-control @error('content') is-invalid @enderror" id="content" name="content:ja" rows="1">{{$expression_ja}}</textarea>
                    @error('content')
                    <p class="text-danger">{{ $message }}</p>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="content">@lang('Spanish'):</label>
                    <textarea class="form-control @error('content') is-invalid @enderror" id="content" name="content:sp" rows="1">{{$expression_sp}}</textarea>
                    @error('content')
                    <p class="text-danger">{{ $message }}</p>
                    @enderror
                </div>

                <button type="submit" class="btn btn-success">@lang('Translate')</button>
            </form>
        @endif
    @endcan

    <p><a href="{{route('expressions.index')}}">@lang('Back')</a></p>

@endsection
