<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ExpressionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('expressions')->insert([
            'user_id' => rand(1, 3)
        ]);
        DB::table('expressions')->insert([
            'user_id' => rand(1, 3)
        ]);
        DB::table('expressions')->insert([
            'user_id' => rand(1, 3)
        ]);
        DB::table('expressions')->insert([
            'user_id' => rand(1, 3)
        ]);
        DB::table('expressions')->insert([
            'user_id' => rand(1, 3)
        ]);
        DB::table('expressions')->insert([
            'user_id' => rand(1, 3)
        ]);

        DB::table('expression_translations')->insert([
            'expression_id' => 1,
            'locale' => 'en',
            'content' => 'Hello'
        ]);
        DB::table('expression_translations')->insert([
            'expression_id' => 1,
            'locale' => 'de',
            'content' => 'Hallo'
        ]);


        DB::table('expression_translations')->insert([
            'expression_id' => 2,
            'locale' => 'ru',
            'content' => 'Как дела?'
        ]);
        DB::table('expression_translations')->insert([
            'expression_id' => 2,
            'locale' => 'ja',
            'content' => 'お元気ですか？'
        ]);
        DB::table('expression_translations')->insert([
            'expression_id' => 2,
            'locale' => 'fr',
            'content' => 'Comment ça va?'
        ]);
        DB::table('expression_translations')->insert([
            'expression_id' => 2,
            'locale' => 'sp',
            'content' => '¿Cómo estás?'
        ]);


        DB::table('expression_translations')->insert([
            'expression_id' => 3,
            'locale' => 'en',
            'content' => 'Hello world'
        ]);
        DB::table('expression_translations')->insert([
            'expression_id' => 3,
            'locale' => 'ru',
            'content' => 'Привет мир'
        ]);
        DB::table('expression_translations')->insert([
            'expression_id' => 3,
            'locale' => 'sp',
            'content' => 'Hola Mundo'
        ]);


        DB::table('expression_translations')->insert([
            'expression_id' => 4,
            'locale' => 'ru',
            'content' => 'Я программист'
        ]);
        DB::table('expression_translations')->insert([
            'expression_id' => 4,
            'locale' => 'fr',
            'content' => 'Je suis un programmeur informatique'
        ]);
        DB::table('expression_translations')->insert([
            'expression_id' => 4,
            'locale' => 'ja',
            'content' => '私はコンピュータープログラマーです'
        ]);


        DB::table('expression_translations')->insert([
            'expression_id' => 5,
            'locale' => 'en',
            'content' => 'Expensive car'
        ]);
        DB::table('expression_translations')->insert([
            'expression_id' => 5,
            'locale' => 'de',
            'content' => 'Teures Auto'
        ]);
        DB::table('expression_translations')->insert([
            'expression_id' => 5,
            'locale' => 'ja',
            'content' => '高級車'
        ]);


        DB::table('expression_translations')->insert([
            'expression_id' => 6,
            'locale' => 'ru',
            'content' => 'Хорошего дня!'
        ]);
        DB::table('expression_translations')->insert([
            'expression_id' => 6,
            'locale' => 'sp',
            'content' => '¡Que tengas un buen día!'
        ]);
        DB::table('expression_translations')->insert([
            'expression_id' => 6,
            'locale' => 'de',
            'content' => 'Einen schönen Tag noch!'
        ]);

        $data = [
            'user_id' => 1,
            'en' => ['content' => 'Kitchen']
        ];
        $expression = \App\Expression::create($data);
    }
}
