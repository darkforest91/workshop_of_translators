<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExpressionTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expression_translations', function (Blueprint $table) {
            $table->id();
            $table->foreignId('expression_id')->constrained()->cascadeOnDelete();
            $table->string('content')->nullable();
            $table->string('locale')->index();
            $table->unique(['expression_id', 'locale']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expression_translations');
    }
}
