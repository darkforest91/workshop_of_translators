<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExpressionTranslation extends Model
{
    public $timestamps = false;

    protected $fillable = ['content'];
}
