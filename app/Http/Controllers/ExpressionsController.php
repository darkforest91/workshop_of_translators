<?php

namespace App\Http\Controllers;

use App\Expression;
use App\Http\Requests\ExpressionRequest;
use Illuminate\Http\Request;

class ExpressionsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->only(['store', 'update', 'show']);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $locale = app()->getLocale();
        $expressions = Expression::all();
        return view('expressions.index', compact('locale', 'expressions'));
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $locale = app()->getLocale();
        $expression = Expression::findOrFail($id);

        $expression_en = $expression->hasTranslation('en') ? $expression->translate('en')->content : null;
        $expression_ru = $expression->hasTranslation('ru') ? $expression->translate('ru')->content : null;
        $expression_de = $expression->hasTranslation('de') ? $expression->translate('de')->content : null;
        $expression_fr = $expression->hasTranslation('fr') ? $expression->translate('fr')->content : null;
        $expression_ja = $expression->hasTranslation('ja') ? $expression->translate('ja')->content : null;
        $expression_sp = $expression->hasTranslation('sp') ? $expression->translate('sp')->content : null;
        return view('expressions.show',
            compact('locale', 'expression', 'expression_en', 'expression_de',
                                  'expression_fr', 'expression_ja', 'expression_sp','expression_ru'));
    }

    /**
     * @param ExpressionRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(ExpressionRequest $request)
    {
        $expression = new Expression();
        $locale = $request->session()->get('locale');
        $expression->translateOrNew($locale)->content = $request->get('content');
        $expression->user_id = $request->user()->id;
        $expression->save();

        return back();
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $expression = Expression::findOrFail($id);
        $expression->update($request->all());

        return redirect(route('expressions.show', ['expression' => $expression]));
    }
}
