<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LanguageSwitcherController extends Controller
{
    /**
     * @param Request $request
     * @param string $local
     * @return \Illuminate\Http\RedirectResponse
     */
    public function switcher(Request $request, string $local)
    {
        $request->session()->put('locale', $local);
        return back();
    }
}
