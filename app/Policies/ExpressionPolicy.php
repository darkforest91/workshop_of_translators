<?php

namespace App\Policies;

use App\Expression;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ExpressionPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User  $user
     * @param  \App\Expression  $expression
     * @return mixed
     */
    public function view(User $user, Expression $expression)
    {
        $locale = app()->getLocale();
        if ($locale == "ru" && $expression->hasTranslation('ru')){
            return true;
        } elseif ($locale == "en" && $expression->hasTranslation('en')) {
            return true;
        }
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User  $user
     * @param  \App\Expression  $expression
     * @return mixed
     */
    public function update(User $user, Expression $expression)
    {
        //
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\Expression  $expression
     * @return mixed
     */
    public function delete(User $user, Expression $expression)
    {
        //
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\User  $user
     * @param  \App\Expression  $expression
     * @return mixed
     */
    public function restore(User $user, Expression $expression)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\Expression  $expression
     * @return mixed
     */
    public function forceDelete(User $user, Expression $expression)
    {
        //
    }

    public function viewIfNotTranslation(User $user, Expression $expression)
    {
        $locale = app()->getLocale();
        if ($locale == "ru" && !$expression->hasTranslation('ru')){
            return true;
        } elseif ($locale == "en" && !$expression->hasTranslation('en')) {
            return true;
        }
    }
}
