<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'language'], function () {
    Auth::routes();

    Route::get('/', 'ExpressionsController@index')->name('home');

    Route::resource('expressions', 'ExpressionsController')->only(['index' , 'show', 'store', 'update']);
});

Route::get('language/{local}', 'LanguageSwitcherController@switcher')->name('language.switcher')
    ->where('local', 'en|ru');
